package com.indiamart.dtos;

import com.indiamart.baseModelAndDto.BaseDTO;

import lombok.Data;

@Data
public class UserDTO extends BaseDTO {

		private String userId;
		private String userEmail;
		private String userPassword;
		private String userFirstName;
		private String userLastName;
		private String userCity;
		private String userZip;
		private String userPhone;
		private String userCountry;
		private String userAddress;
		private String userAddress2;
		
}