package com.indiamart.service;

import com.indiamart.baseModelAndDto.ResponseModelList;
import com.indiamart.dtos.InvoiceDTO;

public interface InvoiceService {

	ResponseModelList<InvoiceDTO> findByInvoiceId(String invoiceId);
	ResponseModelList<InvoiceDTO> save(InvoiceDTO invoiceDTO);
	ResponseModelList<InvoiceDTO> deleteById(String invoiceId);
	ResponseModelList<InvoiceDTO> findAll();
	ResponseModelList<InvoiceDTO> deleteAll();	
	
	
}
