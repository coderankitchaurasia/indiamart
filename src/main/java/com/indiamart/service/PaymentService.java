package com.indiamart.service;

import com.indiamart.baseModelAndDto.ResponseModelList;
import com.indiamart.dtos.PaymentDTO;

public interface PaymentService {
	
	ResponseModelList<PaymentDTO> findByPaymentId(String paymentId);
	ResponseModelList<PaymentDTO> save(PaymentDTO paymentDTO);
	ResponseModelList<PaymentDTO> deleteById(String paymentId);
	ResponseModelList<PaymentDTO> findAll();
	ResponseModelList<PaymentDTO> deleteAll();

}
