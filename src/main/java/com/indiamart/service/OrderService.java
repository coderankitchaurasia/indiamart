package com.indiamart.service;

import com.indiamart.baseModelAndDto.ResponseModelList;
import com.indiamart.dtos.OrderDTO;

public interface OrderService {
	
	ResponseModelList<OrderDTO> findByPaymentId(String orderID);
	ResponseModelList<OrderDTO> save(OrderDTO orderDTO);
	ResponseModelList<OrderDTO> deleteById(String orderID);
	ResponseModelList<OrderDTO> findAll();
	ResponseModelList<OrderDTO> deleteAll();


}
