package com.indiamart.service;

import com.indiamart.baseModelAndDto.ResponseModelList;
import com.indiamart.dtos.UserDTO;

public interface UserService {

	ResponseModelList<UserDTO> findByUserId(String usrId);
	ResponseModelList<UserDTO> save(UserDTO userDTO);
	ResponseModelList<UserDTO> deleteById(String userId);
	ResponseModelList<UserDTO> findAll();
	ResponseModelList<UserDTO> deleteAll();
}
