package com.indiamart.service;

import com.indiamart.baseModelAndDto.ResponseModelList;
import com.indiamart.dtos.ProductimageDTO;

public interface ProductImageService {

	ResponseModelList<ProductimageDTO> findByProductImageId(String prodctImageId);
	ResponseModelList<ProductimageDTO> save(ProductimageDTO productImageDTO);
	ResponseModelList<ProductimageDTO> deleteById(String prodctImageId);
	ResponseModelList<ProductimageDTO> findAll();
	ResponseModelList<ProductimageDTO> deleteAll();
}