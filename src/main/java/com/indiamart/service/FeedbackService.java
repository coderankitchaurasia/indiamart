package com.indiamart.service;

import com.indiamart.baseModelAndDto.ResponseModelList;
import com.indiamart.dtos.FeedbackDTO;

public interface FeedbackService {

	ResponseModelList<FeedbackDTO> findByFeedbackId(String feedbackId);
	ResponseModelList<FeedbackDTO> save(FeedbackDTO feedbackDTO);
	ResponseModelList<FeedbackDTO> deleteById(String feedbackId);
	ResponseModelList<FeedbackDTO> findAll();
	ResponseModelList<FeedbackDTO> deleteAll();	
	
	
}
