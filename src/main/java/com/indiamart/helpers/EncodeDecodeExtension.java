package com.indiamart.helpers;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * @author Anshul
 *
 */
public class EncodeDecodeExtension {

	public static String Encode(String PlainText) {
		byte[] message = PlainText.getBytes(StandardCharsets.UTF_8);
		String encoded = Base64.getEncoder().encodeToString(message);
		return encoded;
	}

	public static String Decode(String CipherText) {
		byte[] decoded = Base64.getDecoder().decode(CipherText);
		return new String(decoded, StandardCharsets.UTF_8);
	}

}
