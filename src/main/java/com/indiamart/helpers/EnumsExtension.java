package com.indiamart.helpers;

public class EnumsExtension {

	public static enum eOrderBy {
		enAsc("Asc", "Ascending Order"), enDesc("Desc", "Descending Order");

		private String firstvalue;
		private String secondValue;

		eOrderBy(String firstvalue, String secondValue) {
			this.firstvalue = firstvalue;
			this.secondValue = secondValue;
		}

		public String getKey() {
			return this.firstvalue;
		}

		public String getValue() {
			return this.secondValue;
		}

		public static String getFirstValue(String value) {
			for (eOrderBy item : eOrderBy.values()) {
				if (item.getValue().equalsIgnoreCase(value))
					return item.getKey();
			}
			return "";
		}

		public static String getSecondValue(String value) {
			for (eOrderBy item : eOrderBy.values()) {
				if (item.getKey().equalsIgnoreCase(value))
					return item.getValue();
			}
			return "";
		}
	}


	public static enum statusType {
		enEn("En", "Enable"), enDis("Dis", "Disable");
		private String firstvalue;
		private String secondvalue;

		private statusType(String firstvalue, String secondvalue) {
			this.firstvalue = firstvalue;
			this.secondvalue = secondvalue;
		}

		public String getKey() {
			return this.firstvalue;
		}

		public String getValue() {
			return this.secondvalue;
		}

		public static String getFirstValue(String value) {
			for (statusType item : statusType.values()) {
				if (item.getValue().equalsIgnoreCase(value))
					return item.getKey();
			}
			return "";
		}

		public static String getSecondValue(String value) {
			for (statusType item : statusType.values()) {
				if (item.getKey().equalsIgnoreCase(value))
					return item.getValue();
			}
			return "";
		}
	}
}
