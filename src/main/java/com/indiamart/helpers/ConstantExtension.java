package com.indiamart.helpers;

public class ConstantExtension {

	/**
	 * Common Messages Starts
	 */
	public static final String ERROR = "Internal Server Error";
	public static final String SUCCESS = "Succesful";
	public static final String ALREADY_EXIST = "Already Exist";
	public static final String NOT_MATCH = "Not Matched";
	public static final String INVALID = "invalid";
	/**
	 * Common Messages Ends
	 */


	public static final String FILE_UPLOAD_ERROR = "File Upload Error";
	public static String FolderPath = new AppExtension().getFolderPath();
	public static String FolderPathGenre = FolderPath + "data/genre/";

	public static String DatabasePathGenre = "data/genre/";

	public static String SUCCESS_UPDATED = "File Successfully Added";
	/*
		User
	*/
	public static final String USER_ADDED = "User Successfully Added";
	public static final String USER_EXISTS= "User doesn't exists";
	public static final String USER_UPDATED = "User Successfully Updated";
	public static final String USER_SUCCESS_RECEIVE = "User Successfully fetch";
	public static final String USER_NAME_ALREADY_EXIST = "User Name Already Exist";
	public static final String USER_EMAIL_ALREADY_EXIST = "User Email Already Exist";
	public static final String USER_DELETED = "User Successfully Deleted";
	public static final String USER_DISABLED = "User Successfully Disabled";
	public static final String USER_STATUS_CHANGED = "User Status Changed";
	public static final String USER_EMAIL_EXISTS="User EmailId Already Exists";
	public static final String USER_PHONE_NO1="User Contact No1 Already Exists";
	public static final String USER_PHONE_NO2="User Contact No2 Already Exists";
	
	/*
	Product
	 */

	
	public static final String PRODUCT_ADDED = "User Successfully Added";
	public static final String PRODUCT_EXISTS= "User doesn't exists";
	public static final String PRODUCT_UPDATED = "User Successfully Updated";
	public static final String PRODUCT_SUCCESS_RECEIVE = "User Successfully fetch";
	public static final String PRODUCT_DELETED = "User Successfully Deleted";
	
	

	public static final String IMAGE_ADDED = "Image Successfully Added";
	public static final String IMAGE_UPDATED = "Image Successfully Updated";
	public static final String IMAGE_DELETED = "Image Successfully Deleted";
	public static final String IMAGE_SUCCESS_RECEIVE = "Image Successfully fetch";
	public static final String IMAGE_NAME_ALREADY_EXIST = "Image Name Already Exist";

	public static final String RATING_ADDED = "Rating Successfully Added";
	public static final String RATING_UPDATED = "Rating Successfully Updated";
	public static final String RATING_DELETED = "Rating Successfully Deleted";
	public static final String RATING_SUCCESS_RECEIVE = "Rating Successfully fetch";
	public static final String RATING_ALREADY_EXIST = "Rating Already Exist";

	public static final String REVIEW_ADDED = "Review Successfully Added";
	public static final String REVIEW_UPDATED = "Review Successfully Updated";
	public static final String REVIEW_DELETED = "Review Successfully Deleted";
	public static final String REVIEW_SUCCESS_RECEIVE = "Review Successfully fetch";
	public static final String REVIEW_NAME_ALREADY_EXIST = "Review Name Already Exist";


	public static final String PASSWORD_UPDATED = "Password Successfully Updated";
	public static final String PASSWORD_INVALID = "Password Invalid";

	/*
		Service Constant
	*/
	public static final String SERVICE_ALREADY="Service Already Exists";
	public static final String SERVICE_UPDATED="Service Successfully Updated";
	public static final String SERVICE_ADDED="Service Successfully Added";
	public static final String SERVICE_DELETED="Service Successfully Deleted";
	public static final String SERVICE_FETCHED="Service Successfully Fetched";
	public static final String SERVICE_WRONG="Wrong Serice Id";

	/*
		Feedback
	*/
	public static final String FEEDBACK_ADDED="Feedback Successfully Added";
	public static final String FEEDBACK_UPDATED="Feedback Successfully Updated";
	public static final String FEEDBACK_FETCHED="Feedback Successfully Fetched";
	public static final String FEEDBACK_DELETED="Feedback Successfully Deleted";
	public static final String FEEDBACK_ID_WRONG="Feedback Id Is Wrong";
	public static final String FEEDBACK_ENTRIES="No Feedback available";
	public static final String FEEDBACK_CLIENT="Feedback Client id is wrong";

	/*
	 * Client
	*/

	public static final String CLIENT_ADDED = "Client Successfully Added";
	public static final String CLIENT_UPDATED = "Client Successfully Updated";
	public static final String CLIENT_SUCCESS_RECEIVE = "Client Successfully fetch";
	public static final String CLIENT_NAME_ALREADY_EXIST = "Client Name Already Exist";
	public static final String CLIENT_EMAIL_ALREADY_EXIST = "Client Email Already Exist";
	public static final String CLIENT_DELETED = "Client Successfully Deleted";
	public static final String CLIENT_DISABLED = "Client Successfully Disabled";
	public static final String CLIENT_STATUS_CHANGED = "Client Status Changed";
	public static final String CLIENT_EMAIL_EXISTS="Client EmailId Already Exists";
	public static final String CLIENT_PHONE_NO1="Client Contact No1 Already Exists";
	public static final String CLIENT_PHONE_NO2="Client Contact No2 Already Exists";
	public static final String CLIENT_PHONE_NOT_FOUND="Contact didn't match";
	public static final String CLIENT_NOT_FOUND="Client not found";

	/*
	 * Employee
	*/
	public static final String EMPLOYEE_ADDED = "Employee Successfully Added";
	public static final String EMPLOYEE_UPDATED = "Employee Successfully Updated";
	public static final String EMPLOYEE_SUCCESS_RECEIVE = "Employee Successfully fetch";
	public static final String EMPLOYEE_NAME_ALREADY_EXIST = "Employee Name Already Exist";
	public static final String EMPLOYEE_EMAIL_ALREADY_EXIST = "Employee Email Already Exist";
	public static final String EMPLOYEE_DELETED = "Employee Successfully Deleted";
	public static final String EMPLOYEE_DISABLED = "Employee Successfully Disabled";
	public static final String EMPLOYEE_STATUS_CHANGED = "Employee Status Changed";
	public static final String EMPLOYEE_EMAIL_EXISTS="Employee EmailId Already Exists";
	public static final String EMPLOYEE_PHONE_NO1="Employee Contact No1 Already Exists";
	public static final String EMPLOYEE_PHONE_NO2="Employee Contact No2 Already Exists";
	public static final String EMPLOYEE_PHONE_NOT_FOUND="Employee didn't match";
	public static final String EMPLOYEE_NOT_FOUND="Employee not found";

	/*
		Department
	*/
	public static final String DEPARTMENT_ADDED = "Department Successfully Added";
	public static final String DEPARTMENT_UPDATED = "Department Successfully Updated";
	public static final String DEPARTMENT_SUCCESS_RECEIVE = "Department Successfully fetch";
	public static final String DEPARTMENT_NAME_ALREADY_EXIST = "Department Already Exist";
	public static final String DEPARTMENT_DELETED = "Department Successfully Deleted";
	public static final String DEPARTMENT_STATUS_CHANGED = "Department Status Changed";
	public static final String DEPARTMENT_NOT_MATCH="Department didn't match";
	public static final String DEPARTMENT_NOT_FOUND="Department not found";

	/*
		Designation
	*/
	public static final String DESIGNATION_ADDED = "Designation Successfully Added";
	public static final String DESIGNATION_UPDATED = "Designation Successfully Updated";
	public static final String DESIGNATION_SUCCESS_RECEIVE = "Designation Successfully fetch";
	public static final String DESIGNATION_NAME_ALREADY_EXIST = "Designation Already Exist";
	public static final String DESIGNATION_DELETED = "Designation Successfully Deleted";
	public static final String DESIGNATION_STATUS_CHANGED = "Designation Status Changed";
	public static final String DESIGNATION_NOT_MATCH="Designation didn't match";
	public static final String DESIGNATION_NOT_FOUND="Designation not found";


	/*
COMPANY
*/
public static final String COMPANY_ADDED="Company Successfully Added";
public static final String COMPANY_UPDATED="Company Successfully Updated";
public static final String COMPANY_FETCHED="Company Successfully Fetched";
public static final String COMPANY_DELETED="Company Successfully Deleted";
public static final String COMPANY_ID_WRONG="Company Id Is Wrong";
public static final String COMPANY_ENTRIES="No Company available";



/*
Mail
*/
public static final String MAIL_SENT="Mail Successfully sent";
public static final String MAIL_UPDATED="Mail Successfully Updated";
public static final String MAIL_FETCHED="Mail Successfully Fetched";
public static final String MAIL_DELETED="Mail Successfully Deleted";
public static final String MAIL_ID_WRONG="Mail Id Is Wrong";
public static final String MAIL_ENTRIES="No Mail available";
public static final String MAIL_ERROR="Mail is not send";



}