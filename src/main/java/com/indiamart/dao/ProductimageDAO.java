package com.indiamart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indiamart.model.Productimage;

@Repository
public interface ProductimageDAO extends JpaRepository<Productimage, String> {

}
