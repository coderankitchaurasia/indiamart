package com.indiamart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indiamart.model.Complatint;

@Repository
public interface ComplatintDAO extends JpaRepository<Complatint, String> {

}
