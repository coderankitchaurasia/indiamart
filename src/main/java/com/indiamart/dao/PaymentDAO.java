package com.indiamart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indiamart.model.Payment;

@Repository
public interface PaymentDAO extends JpaRepository<Payment, String>{

}
