package com.indiamart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indiamart.model.Invoice;

@Repository
public interface InvoiceDAO extends JpaRepository<Invoice, String>{

}
