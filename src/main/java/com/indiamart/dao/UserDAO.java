package com.indiamart.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indiamart.model.User;



@Repository
public interface UserDAO extends JpaRepository<User, String>{


	List<User> findByUserEmail(String email);

	List<User> findByIsFlag(int i);


}
