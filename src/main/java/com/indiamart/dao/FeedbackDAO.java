package com.indiamart.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.indiamart.model.Feedback;

@Repository
public interface FeedbackDAO extends JpaRepository<Feedback, String>{

}
