package com.indiamart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.indiamart.baseModelAndDto.ResponseModelList;
import com.indiamart.dtos.ProductDTO;
import com.indiamart.dtos.UserDTO;
import com.indiamart.service.ProductService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("product")
public class ProductController {

	ResponseModelList<ProductDTO> responseModel = new ResponseModelList<ProductDTO>();
	
	@Autowired
	ProductService product;
	
	@GetMapping("/findById/{productId}")
	public ResponseModelList<ProductDTO> findByUserId(@PathVariable("productId") String productId) {
		return product.findByProductId(productId);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseModelList<ProductDTO> save(@RequestBody ProductDTO productDTO) {
		return product.save(productDTO);
	}
	
	
	@GetMapping("findAll")
	ResponseModelList<ProductDTO> findAll() {
		ResponseModelList<ProductDTO> responseModelList = new ResponseModelList<>();
		try {
			log.info("fetching User Information");
			responseModelList = product.findAll();
		} catch (Exception e) {
			log.info("Error while saving Product Information   ###  " + getClass().getName());
			e.printStackTrace();
		}
		return responseModelList;
	}

	
	
	
	
	
}
