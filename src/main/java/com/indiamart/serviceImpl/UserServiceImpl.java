package com.indiamart.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indiamart.baseModelAndDto.ResponseModelList;
import com.indiamart.dao.UserDAO;
import com.indiamart.dtos.UserDTO;
import com.indiamart.helpers.ConstantExtension;
import com.indiamart.helpers.HelperExtension;
import com.indiamart.model.User;
import com.indiamart.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDAO dao;

	ResponseModelList<UserDTO> responseModel = new ResponseModelList<UserDTO>();
	HelperExtension helperExtension = new HelperExtension();
	ModelMapper mapper = new ModelMapper();

	private boolean status = false;
	private String message = "";
	private List<UserDTO> list = null;

	@Override
	public ResponseModelList<UserDTO> findByUserId(String userId) {
		list = new ArrayList<UserDTO>();
		try {
			User user = dao.findById(userId).get();
			if (user != null) {
				UserDTO userDTO = mapper.map(user, UserDTO.class);
				list.add(userDTO);
				responseModel = new ResponseModelList<>(true, ConstantExtension.USER_SUCCESS_RECEIVE, list);
			} else {
				responseModel = new ResponseModelList<>(false, ConstantExtension.USER_EXISTS, null);
			}
		} catch (Throwable e) {
			log.error("Error while finding user information by id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<UserDTO> save(UserDTO userDTO) {
		try {
			list = new ArrayList<>();
			log.info("Saving User information");

			if (!helperExtension.isNullOrEmpty(userDTO.getUserId())) {
				putValueInResponseModel(true, ConstantExtension.USER_UPDATED, userDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.USER_ADDED, userDTO);
			}
			log.info(message);
			responseModel = new ResponseModelList<>(status, message, list);
		} catch (Throwable e) {
			log.error("error while saving user information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	@Override
	public ResponseModelList<UserDTO> deleteById(String userId) {
		list = new ArrayList<UserDTO>();
		try {
			User user = dao.findById(userId).get();
			if (user != null) {
				user.setIsFlag(0);
				user = dao.save(user);
				UserDTO userDTO = mapper.map(user, UserDTO.class);
				list.add(userDTO);
				responseModel = new ResponseModelList<>(true, ConstantExtension.USER_DELETED, list);
			} else {
				responseModel = new ResponseModelList<>(false, ConstantExtension.USER_EXISTS, null);
			}
		} catch (Throwable e) {
			log.error("Error while finding user information by id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<UserDTO> findAll() {
		list = new ArrayList<UserDTO>();
		
		try {
			List<User> user = dao.findAll();
			if (user != null && user.size() > 0) {
				for (User user2 : user)
					list.add(mapper.map(user2, UserDTO.class));
				responseModel = new ResponseModelList<>(true, ConstantExtension.USER_SUCCESS_RECEIVE, list);
			} else {
				responseModel = new ResponseModelList<>(false, ConstantExtension.USER_EXISTS, null);
			}
		} catch (Throwable e) {
			log.error("Error while finding user information by id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	@Override
	public ResponseModelList<UserDTO> deleteAll() {
		list = new ArrayList<UserDTO>();
		try {
			List<User> user = dao.findAll();
			if (user != null && user.size() > 0) {
				for (User user2 : user) {
					user2.setIsFlag(0);
					user2 = dao.save(user2);
					list.add(mapper.map(user2, UserDTO.class));
				}
				responseModel = new ResponseModelList<>(true, ConstantExtension.USER_DELETED, list);
			} else {
				responseModel = new ResponseModelList<>(false, ConstantExtension.USER_EXISTS, null);
			}
		} catch (Throwable e) {
			log.error("Error while finding user information by id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}

	private void putValueInResponseModel(boolean status, String message, UserDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		if (!helperExtension.isNullOrEmpty(dto)) {
			User model = mapper.map(dto, User.class);
			if (!helperExtension.isNullOrEmpty(dto.getUserId())) {
				model.setUserId(dto.getUserId());
				model.setUpdatedOn(dateTime);
			} else {
				model.setUserId("user" + helperExtension.getUniqueId());
				model.setCreatedOn(dateTime);
				model.setUpdatedOn(dateTime);
			}
			model = dao.save(model);
			dto = mapper.map(model, UserDTO.class);
			list.add(dto);
		}
	}

}
