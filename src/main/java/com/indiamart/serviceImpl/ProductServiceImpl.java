package com.indiamart.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indiamart.baseModelAndDto.ResponseModelList;
import com.indiamart.dao.ProductDAO;
import com.indiamart.dtos.ProductDTO;
import com.indiamart.dtos.UserDTO;
import com.indiamart.helpers.ConstantExtension;
import com.indiamart.helpers.HelperExtension;
import com.indiamart.model.Product;
import com.indiamart.model.User;
import com.indiamart.service.ProductService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	ProductDAO productdao;
	
	ResponseModelList<ProductDTO> responseModel = new ResponseModelList<ProductDTO>();
	HelperExtension helperExtension = new HelperExtension();
	ModelMapper mapper = new ModelMapper();

	private boolean status = false;
	private String message = "";
	private List<ProductDTO> list = null;
		
	
	@Override
	public ResponseModelList<ProductDTO> findByProductId(String productId) {
		list = new ArrayList<ProductDTO>();
		try {
			Product product = productdao.findById(productId).get();
			if (product != null) {
				ProductDTO productDTO = mapper.map(product, ProductDTO.class);
				list.add(productDTO);
				responseModel = new ResponseModelList<>(true, ConstantExtension.PRODUCT_SUCCESS_RECEIVE, list);
			} else {
				responseModel = new ResponseModelList<>(false, ConstantExtension.PRODUCT_EXISTS, null);
			}
		} catch (Throwable e) {
			log.error("Error while finding user information by id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}
	
	
	
	@Override
	public ResponseModelList<ProductDTO> save(ProductDTO ProductDTO) {
		try {
			list = new ArrayList<>();
			log.info("Saving Product information");

			if (!helperExtension.isNullOrEmpty(ProductDTO.getProductId())) {
				putValueInResponseModel(true, ConstantExtension.PRODUCT_UPDATED, ProductDTO);
			} else {
				putValueInResponseModel(true, ConstantExtension.PRODUCT_ADDED, ProductDTO);
			}
			log.info(message);
			responseModel = new ResponseModelList<>(status, message, list);
		} catch (Throwable e) {
			log.error("error while saving product information");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}

		return responseModel;
	}

	
	
	@Override
	public ResponseModelList<ProductDTO> deleteById(String productID) {
		// TODO Auto-generated method stub
		return null;
	}


	
	
	
	@Override
	public ResponseModelList<ProductDTO> findAll() {
		list = new ArrayList<ProductDTO>();
		
		try {
			List<Product> product = productdao.findAll();
			if (product != null && product.size() > 0) {
				for (Product product1 : product)
					list.add(mapper.map(product1, ProductDTO.class));
				responseModel = new ResponseModelList<>(true, ConstantExtension.PRODUCT_SUCCESS_RECEIVE, list);
			} else {
				responseModel = new ResponseModelList<>(false, ConstantExtension.PRODUCT_EXISTS, null);
			}
		} catch (Throwable e) {
			log.error("Error while finding product information by id");
			e.printStackTrace();
			responseModel = new ResponseModelList<>(false, ConstantExtension.ERROR, null);
		}
		return responseModel;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public ResponseModelList<ProductDTO> deleteAll() {
		// TODO Auto-generated method stub
		return null;
	}
	

	private void putValueInResponseModel(boolean status, String message, ProductDTO dto) {
		this.status = status;
		this.message = message;
		Date dateTime = helperExtension.getDateTime();
		if (!helperExtension.isNullOrEmpty(dto)) {
			Product model = mapper.map(dto, Product.class);
			if (!helperExtension.isNullOrEmpty(dto.getProductId())) {
				model.setProductId(dto.getProductId());
				model.setUpdatedOn(dateTime);
			} else {
				model.setProductId("product" + helperExtension.getUniqueId());
				model.setCreatedOn(dateTime);
				model.setUpdatedOn(dateTime);
			}
			model = productdao.save(model);
			dto = mapper.map(model, ProductDTO.class);
			list.add(dto);
		}
	}
	

}
