// default package
// Generated May 21, 2020 10:11:13 PM by Hibernate Tools 5.2.10.Final
package com.indiamart.model;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Addtocart generated by hbm2java
 */
@Entity
@Table(name = "addtocart", catalog = "indiamart")
public class Addtocart extends BaseModel implements java.io.Serializable {

	private String cartId;
	private Integer status;
	private Integer count;
	private List<Order> orders = new ArrayList<Order>(0);
	private List<Product> products=new ArrayList<Product>(0);
	
	public Addtocart() {
	}

	public Addtocart(String cartId) {
		this.cartId = cartId;
	}

	public Addtocart(String cartId, Integer status, Integer count, List<Order> orders) {
		this.cartId = cartId;
		this.status = status;
		this.count = count;
		this.orders = orders;
	}

	@Id
	@Column(name = "cart_id", unique = true, nullable = false, length = 21)
	public String getCartId() {
		return this.cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name = "count")
	public Integer getCount() {
		return this.count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "addtocart")
	public List<Order> getOrders() {
		return this.orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	
	
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "cart_product", catalog = "indiamart", joinColumns = {
			@JoinColumn(name = "cart_id", nullable = false, updatable = false) },
			inverseJoinColumns = { @JoinColumn(name = "product_id",
					nullable = false, updatable = false) })
	public List<Product> getProducts() {
		return this.products;
	}
	
	public void setProducts(List<Product> products) {
		 this.products=products;
	}

}
